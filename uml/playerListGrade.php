<?php 
	if(!defined('APP')) die('error!');
	header('content-type:text/html;charset=utf-8');
?>
<!!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>选手成绩表</title>
		<link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="js/vue.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>	
		<div id="app">
			<div class="container">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title text-center">选手成绩表</h3>
					</div>
					<div class="panel-body">
						<div class="row" style="margin-top: 50px;">
							<table border="1" cellspacing="0" align="center" >
								<tr>
									<th>ID</th><th>姓名</th><th>地区</th><th>分数</th>
								</tr>
								<?php if(!empty($emp_info)){ ?>
								<?php foreach($emp_info as $row){ ?>
								<tr>
									<td><?php echo $row['playerId']; ?></td>
									<td><?php echo $row['playerName']; ?></td>
									<td><?php echo $row['region']; ?></td>
									<td><?php echo $row['grade']; ?></td>
								</tr>
								<?php } ?>
								<?php }else{ ?>
									<tr><td colspan="6">暂无选手数据!</td></tr>
								<?php } ?>

							</table>
                            <a href="index.html" >返回</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>