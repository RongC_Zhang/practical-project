<?php
	//声明文件解析的编码格式
	header('content-type:text/html;charset=utf-8');//浏览器编码格式设置
	//连接数据库
	$link = mysqli_connect('localhost','root','');
	mysqli_query($link,"set names 'utf8'");//数据库读取的数据 --编码格式设置
	//判断数据库连接是否成功，如果不成功则显示错误信息并终止脚本继续执行
	if(!$link){
		die('连接数据库失败!'.mysqli_error());
	}
	//选择users数据库
	mysqli_select_db($link,"uml");
	//准备SQL语句
	$sql = "select * from playertable";
	//执行SQL语句，获取结果集
	$result = mysqli_query($link,$sql);
	//定义员工数组，用以保存员工信息
	$emp_info = array();
	

	//遍历结果集，获取每位员工的详细数据
	while($row = mysqli_fetch_assoc($result)){
		$emp_info[] = $row;
	}
	//设置常量，用以判断视图文件是否由此文件加载
	define('APP','itcast');
	//加载HTML模板文件，显示数据
	require './playerListGrade.php';
	?>